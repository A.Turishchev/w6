function check(){
    let kol = document.getElementById("kol").value;
    if(kol.match(/^[0-9]+$/) === null){
        return false;
    }
    else
    {
        return true;
    }
}
function getPrices() {
    return {
        prodType: [55, 200, 400],
        prodOptions: {
            option1 : 0,
            option2: 25,
            option3: 50,
        },
        prodProperties: {
            prop1: 50,
            prop2: 100,
        },
    };
}
function price(){
    let sel = document.getElementById("prodType");
    let kol = document.getElementById("kol").value;
    let radios = document.getElementById("radios");
    let boxes = document.getElementById("checkboxes");
    let price = document.getElementById("price");
    switch (sel.value) {
        case "1":
            radios.style.display = "none";
            boxes.style.display = "none";
            if(check()){
                let all = parseInt(kol) * getPrices().prodType[0];
                document.getElementById("result").innerHTML = "Стоимость равна: " + all + " р";
            }
            else 
                {
document.getElementById("result").innerHTML = "Ошибка! Нужно ввести целое число "
          }
            break;
            
        case "2":
            radios.style.display = "block";
            boxes.style.display = "none";
            let a;
            let regKol = document.getElementsByName("prodOptions");
            for (var i = 0; i < regKol.length; i++) {
                if (regKol[i].checked){
                    a = regKol[i].value;
                }
            }
            let stoim = getPrices().prodOptions[a];
            if(check()){
                let all = parseInt(kol)*(getPrices().prodType[1] + stoim);
                document.getElementById("result").innerHTML = "Стоимость равна: " + all + " р";
            }
               else 
                {
document.getElementById("result").innerHTML = "Ошибка! Нужно ввести целое число "        
                }
            break;
            
        case "3":
            radios.style.display = "none";
            boxes.style.display = "block";
            let sumOp = 0;
            let p = document.getElementsByName("prop");
            for (let i = 0; i < p.length ; i++) {
                if(p[i].checked){
                    sumOp += getPrices().prodProperties[p[i].value];
                }
            }
            if(check()){
                let all = parseInt(kol) * (getPrices().prodType[2] + sumOp);
                document.getElementById("result").innerHTML = "Стоимость равна: " + all + " р";
               
            }
               else 
                {
document.getElementById("result").innerHTML = "Ошибка! Нужно ввести целое число "        
                }
            break;
            
    }
}
window.addEventListener("DOMContentLoaded", function (event) {
    let radios = document.getElementById("radios");
    radios.style.display = "none";
    let boxes = document.getElementById("checkboxes");
    boxes.style.display = "none";

    let kol = document.getElementById("kol");
    kol.addEventListener("change", function (event) {
        price();
    });

    let select = document.getElementById("prodType");
    select.addEventListener("change", function (event) {
        price();
    });

    let pr = document.getElementsByName("prodOptions");
    pr.forEach(function (radio) {
        radio.addEventListener("change", function (event) {
            price();
        });
    });
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener("change", function (event) {
            price();
        });
    });
    price();
});
